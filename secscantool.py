#!/usr/bin/env python3

import click
import os
import subprocess


def get_tool_choice():

    click.secho(
        click.style(
            """
    █▀ █▀▀ █▀▀   █▀ █▀▀ ▄▀█ █▄░█   ▀█▀ █▀█ █▀█ █░░
    ▄█ ██▄ █▄▄   ▄█ █▄▄ █▀█ █░▀█   ░█░ █▄█ █▄█ █▄▄

    Please select a security scan tool:

    1. Snyk
    2. SonarQube
    """,
            fg="cyan",
        )
    )

    tool_choice = click.prompt(
        "",
        type=int,
    )
    return tool_choice


def run_snyk(option=None):
    if option is None:
        click.secho(
            click.style(
                "Choose a Snyk Option:",
                fg="cyan",
            )
        )
        option = click.prompt(
            "",
            type=click.Choice(["test", "monitor", "wizard"]),
        )

    try:
        if option == "test":
            subprocess.check_call(["snyk", "test"])

        elif option == "monitor":
            subprocess.check_call(["snyk", "monitor"])

        elif option == "wizard":
            subprocess.check_call(["snyk", "wizard"])

    except subprocess.CalledProcessError as e:
        click.secho(f"An Exception occurred running Snyk: {e.output}", fg="cyan")


def run_sonarqube():
    project_key: str = os.getenv("SONAR_PROJECT_KEY", None)
    sources: str = os.getenv("SONAR_SOURCE", None)
    host_url: str = os.getenv("SONAR_HOST_URL", None)
    login_token: str = os.getenv("SONAR_LOGIN_TOKEN", None)

    if None in (project_key, sources, host_url, login_token):
        raise click.UsageError("Environment Variables not set!")

    try:
        subprocess.check_call(
            [
                "sonar-scanner",
                f"-Dsonar.projectKey={project_key}",
                f"-Dsonar.sources={sources}",
                f"-Dsonar.host.url={host_url}",
                f"-Dsonar.login={login_token}",
            ]
        )

    except subprocess.CalledProcessError as e:
        click.secho(
            f"An Exception occurred running Sonar Scanner: {e.output}", fg="cyan"
        )


@click.command()
@click.option("--scan", required=False, type=click.Choice(["snyk", "sonar-scanner"]))
@click.option(
    "--option", required=False, type=click.Choice(["test", "monitor", "wizard"])
)
def cli(scan, option):
    if not scan:
        tools = {1: run_snyk, 2: run_sonarqube}
        tool_choice = get_tool_choice()
        tools[tool_choice]()

    if scan == "snyk":
        run_snyk(option=option)

    elif scan == "sonar-scanner":
        if option:
             raise click.UsageError("Cannot use option flag with a Sonar Scanner scan.")
        run_sonarqube()


if __name__ == "__main__":
    cli()
