[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://www.python.org">
    <img src="images/Python.png" alt="Python" width="400" height="175">
  </a>

  <h3 align="center">SecScanTool</h3>

  <p align="center">
    A Minimal Security Scanning Tool to Run Your Favorite Scanners From One Place
    <br />
    <a href="https://gitlab.com/antonino.lopez/secscantool/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/antonino.lopez/secscantool/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<!-- ABOUT THE PROJECT -->
## About The Project
<p align="center">
  <a href="https://gitlab.com/antonino.lopez/secscantool">
    <img src="images/screenshot.png" alt="Python" width="621" height="400">
  </a>
</p>

### Built With

* [Python 3](https://www.python.org)
* [Click](https://click.palletsprojects.com/en/7.x/)

<!-- GETTING STARTED -->
## Getting Started

To get SecScanTool up and running locally, [Python3.5<](https://www.python.org/downloads/release/python-374/) must be installed and added to PATH.  

### Prerequisites

Additional prerequisites to get SecScanTool up and running.

* Update pip3 for most Linux ditros

```sh
sudo -H pip3 install --upgrade pip
```

### Installation

1. Clone the repo

```sh
git clone https:://github.com/nlopez99/PyNmapGUI.git
```

2. Pip install packages

```sh
pip install -r requirements.txt
```
<!-- USAGE EXAMPLES -->
## Usage

Example of running SecScanTool:

```sh
sudo chmod +x secscantool.py
```

```sh
./secscantool --scan sonar-scanner
```

```sh
./secscantool --scan snyk --option test
```

<!-- ROADMAP -->
## Roadmap

See the [open issues]("https://gitlab.com/antonino.lopez/secscantool/issues") for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- CONTACT -->
## Contact

Project Link: ["https://gitlab.com/antonino.lopez/secscantool/]("https://gitlab.com/antonino.lopez/secscantool/)

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)

<!-- MARKDOWN LINKS & IMAGES -->
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=flat-square
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/nino-lopez-tampa/
[product-screenshot]: images/screenshot.png