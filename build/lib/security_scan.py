import click
import os
import subprocess


def get_tool_choice():

    click.secho(
        click.style(
            """
    █▀ █▀▀ █▀▀   █▀ █▀▀ ▄▀█ █▄░█   ▀█▀ █▀█ █▀█ █░░
    ▄█ ██▄ █▄▄   ▄█ █▄▄ █▀█ █░▀█   ░█░ █▄█ █▄█ █▄▄

    Please select a security scan tool:

    1. Snyk
    2. SonarQube
    """,
            fg="cyan",
        )
    )

    tool_choice = click.prompt(
        "",
        type=int,
    )
    return tool_choice


def run_snyk():
    pass


def run_sonarqube():
    env: os.environ = os.environ.copy()
    project_key: str = env["PROJECT_KEY"]
    host_url: str = env["HOST_URL"]
    login_token: str = env['LOGIN_TOKEN']

    subprocess.check_call(['sonar-scanner', f'-Dsonar.projectKey={project_key}', f'-Dsonar.host.url={host_url}', f'-Dsonar.login={login_token}'])


def cli():
    tools = {"1": run_snyk(), "2": run_sonarqube()}
    tool_choice = get_tool_choice()

    if tool_choice == 2:
        tools[tool_choice]
        click.secho(click.style("You chose Snyk", fg='cyan'))

if __name__ == "__main__":
    cli()
    


# check for env file or JSON file

# choose security scanner tool

# run selected tool
