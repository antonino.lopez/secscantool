from setuptools import setup,  find_packages

setup(
    name='secscantool',
    version='0.1',
    py_modules=['security_scan'],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        security_scan=security_scan:cli
    ''',
)